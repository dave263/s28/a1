

// fetch(`https://jsonplaceholder.typicode.com/todos`)
// .then( response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos`)
// .then(response => response.json())
// .then(response => {
//     response.map(element => {
//         console.log(response)
//     })
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`)
// .then( response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`)
// .then( response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//     method: "POST",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//         title: "New To Do",
//         body: "Here is your to do",
//         userId: 1
//     })
// })
// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//         method: "PUT",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify({
//             title: "Updated Post",
//             body: "Hello updated post!",
//             userId: 1
//         })
// })
// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//         method: "PUT",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify({
//             title: "Another title",
//             body: "Another description",
//             status: "New Status",
//             datecompleted: "today",
//             userId: 2
//         })
// })
// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })


// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//     method: "PATCH",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//         title: "Corrected Post thru patch method"
//     })
// })
// .then(data => data.json())
// .then(data => {
//     console.log(data)
// })


// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//         method: "PUT",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify({
//             status: "completed",
//             datechanged: "today"
//         })
// })
// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })

// fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
//     method: "DELETE"
// })

